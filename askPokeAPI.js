const pokeAPI = {};

const https = require('https');

function askPokeAPI(analizedData, callback){

  let response = {};
  response.exit = false;
  response.text = "";

  if(analizedData.isPoke == true){
    if (analizedData.numberOfPoke==0) {
      response.text = analizedData.answer;
      callback(response);
    }
    for (var i = 0; i < analizedData.numberOfPoke; i++) {
      let pokeName = analizedData.pokeList[i];
      let url = 'https://pokeapi.co/api/v2/pokemon/';

      url += pokeName;
      https.get(url,(resp) =>{
        let data = '';

        resp.on('data', (chunk) => {
          data += chunk;
        });

        resp.on('end', () => {
          writeAnswer(analizedData, data, i, pokeName, (result) =>{
            response.text += result+"\n";
          })
        });
      });
    }
    setTimeout(function() {
        callback(response);
        }, 500);
  }else{
        if (analizedData.intent == "Despedida") {
          response.text = analizedData.answer;
          response.exit = true;
        }
        if(analizedData.intent == "No"){
          response.text = "Adios entrenador, éxito en tu aventura";
          response.exit = true;
        }
        else if(analizedData.intent == "Si"){
            response.text = "De acuerdo entrenador, ¿en que puedo ayudarte? c:";
        }else{
          response.text = analizedData.answer;
        }
        callback(response);
  }
}

function writeAnswer(analizedData, pokeAPIData, i, pokeName ,callback){

  var pokeData = JSON.parse(pokeAPIData);
  var intent = analizedData.intent;
  let result = '';

  switch (intent) {
    case "Habilidades":
      result=abilityAnswer(pokeData, pokeName);
      break;
    case "Peso":
      result=weightAnswer(pokeData, pokeName, analizedData);
      break;
    case "Altura":
      result=heightAnswer(pokeData, pokeName, analizedData);
      break;
    case "Estadísticas":
      result=statsAnswer(pokeData, pokeName);
      break;
    case "Movimientos":
      result=moveAnswer(pokeData, pokeName);
      break;
    case "Tipo":
      result=typeAnswer(pokeData, pokeName);
      break;
      default:

  }
  callback(result);

}

function abilityAnswer(pokeData, pokeName){
  result = "Las habilidades de ";
  result += pokeName;
  result += " son ";

  var numAbilities = Object.keys(pokeData.abilities).length

  for (var i = 0; i < numAbilities; i++) {
    result += pokeData.abilities[i].ability.name;

    if (i==numAbilities-2) {
      result += ' y ';
    }else{
      result += ' ';
    }
  }
  return result;
}

function weightAnswer(pokeData, pokeName, analizedData){
  result = "El peso de ";
  result += pokeName;
  if(analizedData.convertion == "Libras"){
    result += " en libras es ";
    result += (pokeData.weight*2.204).toFixed(2);
    result += " lb"
  }else{
    result += " es ";
    result += pokeData.weight;
    result += " Kg";
  }
  return result;
}

function heightAnswer(pokeData, pokeName, analizedData){
  result = "La altura de ";
  result += pokeName;
  switch (analizedData.convertion) {
    case "Pies":
        result += " en pies es ";
        result += (pokeData.height*3.28).toFixed(2);
        result += " ft"
      break;
    case "Pulgadas":
        result += " en pulgadas es ";
        result += (pokeData.height*39.37).toFixed(2);
        result += " in"
      break;
    default:
        result += " es ";
        result += pokeData.height;
        result += " m"
      break;
  }
  return result;
}

function statsAnswer(pokeData, pokeName){
  result = "Las estadísticas de ";
  result += pokeName;
  result += " son:\n";
  for (var i = 0; i < Object.keys(pokeData.stats).length; i++) {
    result += pokeData.stats[i].stat.name;
    result += ": "
    result += pokeData.stats[i].base_stat;
    result += "\n"
  }

  return result;
}

function moveAnswer(pokeData, pokeName){
  result = "Los movimientos que ";
  result += pokeName;
  result += " puede aprender son: \n\n";

  var numAbilities = Object.keys(pokeData.moves).length

  for (var i = 0; i < numAbilities; i++) {
    result += pokeData.moves[i].move.name;

    if (i==numAbilities-2) {
      result += ' y ';
    }else if(i != numAbilities-1){
      result += ', ';
    }
    if(i%6==0 && i>0){
      result += "\n";
    }
  }
  result += "\n";
  return result;
}

function typeAnswer(pokeData, pokeName){

  result = pokeName.charAt(0).toUpperCase() + pokeName.slice(1);
  result += " es tipo ";

  var numTypes = Object.keys(pokeData.types).length;

    result += pokeData.types[0].type.name;
    if(numTypes==2){
      result += "/"
      result += pokeData.types[1].type.name;
    }

  return result;
}


pokeAPI.ask = askPokeAPI;

module.exports = pokeAPI;
