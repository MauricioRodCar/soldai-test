const analizeData = {};

function analize(data, callback){

  var jsonData = JSON.parse(data);

  var response = {};

  var intent = jsonData.current_response.intent_name;
  var answer = jsonData.current_response.message;
  var convertion = "";

  if((intent == "Altura")||(intent == "Peso")||(intent == "Movimientos")
      ||(intent == "Tipo")||(intent == "Habilidades")||(intent == "Estadísticas")) {

      response.isPoke = true;

      if(Object.keys(jsonData.current_response.parameters).length > 0){
        var entities = jsonData.current_response.parameters.entities;
        var numberOfEntities = Object.keys(entities).length;
        var numberOfPoke = 0;
        var pokeList = [];

        for (var i = 0; i < numberOfEntities; i++) {
          var pokeName = jsonData.current_response.parameters.entities[i].name;
          if((pokeName != "Pies") && (pokeName != "Pulgadas") && (pokeName !="Libras")){
            numberOfPoke++;
            pokeList.push(pokeName.toLowerCase());
            response.pokeList = pokeList;
          }else{
            convertion = pokeName;
          }
        }
        response.numberOfPoke = numberOfPoke;
      } else {
          response.numberOfPoke = 0;
        }
      }
      else{
        response.isPoke = false;
      }
      response.intent = intent;
      response.answer = answer;
      response.convertion = convertion;
      callback(response);
    }

analizeData.analize = analize;

module.exports = analizeData;
