const bill = {};

const https = require('https');

function askBill(question, id ,key, callback) {

  let request = 'https://beta.soldai.com//bill-cipher/askquestion?session_id='
  request+= id;
  request+= '&key=';
  request+= key;
  request+= '&log=1&question=';
  request+= question;

  https.get(request,(resp) =>{
    let data = '';

    resp.on('data', (chunk) => {
      data += chunk;
    });

    resp.on('end', () => {
      callback(data);
    });
  });
}

bill.ask = askBill;

module.exports = bill;
