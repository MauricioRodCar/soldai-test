const readline = require('readline');
const bill = require('./askBill.js');
const analize = require('./analizeData.js');
const pokeAPI = require('./askPokeAPI.js');
const KEY = '773782ef8bb4853f447dee2fdb1ae0b30cc6af19';
const pokeDex = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var index = {};
var exit = false;

let greeting =   "\n   Saludos entrenador!, soy la pokedex 2.0, tengo pocos pokemon registrados, y mis";
greeting += "\nfunciones aún están en desarrollo, pero aprendo rápido, te apoyaré en todo lo que";
greeting += "\npueda, te puedo ayudar en algo?\n\n";

let pokeDexText = "->¿Hay algo más en lo que pueda ayudar?\n\n";
let chat;

pokeDex.question(greeting, (question) => {
  consult(question, exit,(response)=>{
    pokeDex.close();
  })
});

function consult(question, exit, callback){
    bill.ask(question,"Labrys",KEY,(response) =>{
      analize.analize(response, (analizedData) =>{
        pokeAPI.ask(analizedData, (answer) =>{
          setTimeout(function() {
              console.log("\n->"+answer.text+"\n");
          }, 500);
          if(answer.exit==false){
            if(answer.text == "De acuerdo entrenador, ¿en que puedo ayudarte? c:"){
              chat="";
            } else {
              chat=pokeDexText;
            }
            setTimeout(function() {
                pokeDex.question(chat, (question) => {
                consult(question, answer.exit, (answe)=>{
                  callback(answer);
                })
              });
            }, 500);
          } else{
                var endChat = {};
                endChat.exit = true;
                callback(endChat);
          }
        });
      });
    });
}

function consultWeb(question, callback){
    bill.ask(question,"Labrys",KEY,(response) =>{
      analize.analize(response, (analizedData) =>{
        pokeAPI.ask(analizedData, (answer) =>{
          callback(answer);
        });
      });
    });
}

index.consult = consult;
module.exports = index;
